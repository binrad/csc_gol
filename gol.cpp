/*
 * CSc103 Project 3: Game of Life
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */
#include <iostream>
#include <fstream>
#include <cstdio>
#include <stdlib.h> // for exit();
#include <getopt.h> // to parse long arguments.
#include <unistd.h> // sleep
#include <vector>
using std::vector;
#include <string>
using std::string;
using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Text-based version of Conway's game of life.\n\n"
"   --seed,-s     FILE     read start state from FILE.\n"
"   --world,-w    FILE     store current world in FILE.\n"
"   --fast-fw,-f  NUM      evolve system for NUM generations and quit.\n"
"   --help,-h              show this message and exit.\n";

size_t max_gen = 0; /* if > 0, fast forward to this generation. */
string wfilename =  "/tmp/gol-world-current"; /* write state here */
FILE* fworld = 0; /* handle to file wfilename. */
string initfilename = "/tmp/gol-world-current"; /* read initial state from here. */

size_t nbrCount(size_t i, size_t j, const vector<vector<bool> >& g);
void update(vector<vector<bool> >& g, const string& fname);// transform the old version of the world into the new one
int initFromFile(const string& fname);// read initial state from file
void dumpState(FILE* f);// write the state to a file
void mainLoop();

char text[3] = ".O";

int main(int argc, char *argv[]) {
	// define long options
	static struct option long_opts[] = {
		{"seed",    required_argument, 0, 's'},
		{"world",   required_argument, 0, 'w'},
		{"fast-fw", required_argument, 0, 'f'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "hs:w:f:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case 's':
				initfilename = optarg;
				break;
			case 'w':
				wfilename = optarg;
				break;
			case 'f':
				max_gen = atoi(optarg);
				break;
			case '?':
				printf(usage,argv[0]);
		}
	}
	mainLoop();
	return 0;
}

void mainLoop() 
{   
    /* update, write, sleep */
    initFromFile(initfilename);
    if (max_gen == 0)
    {
        while (1)
        {
            sleep(1);
            initFromFile(wfilename);
        }
    }
    else 
    {
        for (size_t i = 0; i < max_gen; i++)
        {
            cout << i << endl;
            initFromFile(wfilename);
        }    
    }
}

int initFromFile(const string& fname)
{
    char c;
    bool live_cell = true;
    bool dead_cell = false;
    vector<vector<bool> > old_state;
    vector<bool> row;
    
    FILE* f = fopen(fname.c_str(), "rb");       // convert string into char*

    if (!f) {
    // deal with error by printing a message, or maybe quitting
    // the entire program with a non-zero exit code, like this:
        cout << "Input file doesn't exist" << endl;
        exit(1);
    }
    
    while(fread(&c, sizeof(char), 1, f))
    {
        if (c == '\n')
        {
            old_state.push_back(row);
            row.clear();
        }
        else
        {
            if (c == text[1])
            {
                row.push_back(live_cell);
            }
            else row.push_back(dead_cell);
        }
    }
    fclose(f);
    update(old_state, fname);
    return 1;
}

void update(vector<vector<bool> >& g, const string& fname)
{
    char c;
    size_t nbrcnt;
    FILE* f = fopen(wfilename.c_str(), "wb");       // convert string into char*
    
    for (size_t i = 0; i < g.size(); i++)
    {
        for (size_t j = 0; j < g[i].size(); j++)
        {
            nbrcnt = nbrCount(i, j, g);
            if (g[i][j] == 0)
            {
                if (nbrcnt == 3)
                {
                    c = text[1];
                }
                else 
                {
                    c = text[0];
                }
            }
            else 
            {
                if (nbrcnt == 2 || nbrcnt == 3)
                {
                    c = text[1];
                }
                else 
                {
                    c = text[0];
                }
            }
            fwrite(&c, sizeof(char), 1, f);
        }
        c = '\n';
        fwrite(&c, sizeof(char), 1, f);
    }
    fclose(f);
}
size_t nbrCount(size_t row, size_t col, const vector<vector<bool> >& g)
{
    size_t c0(2), c1(2), c2(2), c3(2), c4(2), c5(2), c6(2), c7(2);
    if (row < 1)
    {
        if (col < 1)
        {
            c0 = g.back().back();
            c2 = g.back()[col+1];
            c3 = g[row].back();
            c5 = g[row+1].back();
        }
        else if (col == g[row].size()-1) 
        {
            c0 = g.back()[col-1];
            c2 = g.back().front();
            c4 = g[row].front();
            c7 = g[row+1].front();
        }
        else
        {
            c0 = g.back()[col-1];
            c2 = g.back()[col+1];
        }
        c1 = g.back()[col];
    }
    else if (row == g.size()-1)
    {
        if (col < 1)
        {
            c0 = g[row-1].back();
            c3 = g[row].back();            
            c5 = g.front().back();
            c7 = g.front()[col+1];
        }
        else if (col == g[row].size()-1)
        {
            c2 = g[row-1].front();
            c4 = g[row].front();
            c5 = g.front()[col-1];
            c7 = g.front().front();
        }
        else 
        {
            c5 = g.front()[col-1];
            c7 = g.front()[col+1];
        }
        c6 = g.front()[col];
    }
    else
    {
        if (col < 1)
        {
            c0 = g[row-1].back();
            c3 = g[row].back();
            c5 = g[row+1].back();
        }
        else if (col == g[row].size()-1)
        {
            c2 = g[row-1].front();
            c4 = g[row].front();
            c7 = g[row+1].front();
        }
    }
    if (c0 == 2) c0 = g[row-1][col-1];
    if (c1 == 2) c1 = g[row-1][col];
    if (c2 == 2) c2 = g[row-1][col+1];
    if (c3 == 2) c3 = g[row][col-1];
    if (c4 == 2) c4 = g[row][col+1];
    if (c5 == 2) c5 = g[row+1][col-1];
    if (c6 == 2) c6 = g[row+1][col];
    if (c7 == 2) c7 = g[row+1][col+1];
    return c0 + c1 + c2 + c3 + c4 + c5 + c6 + c7;
}
